﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contracts
{
    public interface IPacket
    {
        //typ pakietu
        string Type();

        //typ gry, któego dotyczy pakiet
        string GameType();

        //dane przesłane pakietem
        object Data();
    }
}
