﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Contracts
{
    [ServiceContract]
    public interface IServer
    {
        //Ping, czy serwer dzia.
        [OperationContract]
        IPacket Ping();

        //Update, czyli wyślij mój ruch mojemu wrogowi, by drżał i płakał
        [OperationContract]
        void Update(IPacket state);

        //Połącz klienta z użyciem bazy danych. Może zdążymy.
        //Zwraca id klienta
        //@TODO: Powinien być hash hasła, a nie plain text
        [OperationContract]
        int Connect(String login, String password);

        //Czy serwer wie od kogo dostał wywołanie?
        [OperationContract]
        bool Disconnect();

        [OperationContract]
        Dictionary<Int32, Int32> Ranking();

        //Jeśli ID wroga==0, to losujemy z kim gramy.
        [OperationContract]
        bool WannaPlay(Int32 OpponentID);
        

        [OperationContract]
        Dictionary<Int32, String> OnlinePlayers(String game);

        //Metoda to testowania WCF
        [OperationContract]
        String Test(String wtv);
    }
}
