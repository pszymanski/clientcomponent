﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Contracts
{
    [ServiceContract]
    public interface IClient
    {
        [OperationContract]
        void Update(IPacket state);

        [OperationContract]
        IPacket Ping();

        [OperationContract]
        void Send(IPacket state);

        [OperationContract]
        bool IWannaPlay(Int32 OpponentID);

        [OperationContract]
        int Connect(String login, String password, String game);

        [OperationContract]
        void ChangeRanking(String login, String password, String game, int delta);

        [OperationContract]
        Dictionary<Int32, Int32> Ranking();

        [OperationContract]
        String Test(String wtv);

        void SetServer(Server.IServerService server);

        [OperationContract]
        String Whois(int id);

    }
}

