﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contracts
{
    public interface IGame
    {
        void Start();
        void Start(IPacket state);
        void Update(IPacket state);
    }
}
