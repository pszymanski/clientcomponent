Komponenty klient-serwer WCF.
=============================

Ostateczne dll jest kopiowana do głównego katalogu solucji.

SERWER
------
Serwer odpala się w konsoli za pomocą ServerHost.exe

KLIENT
------
Komponent klienta znajduje się w WcfClient.dll

Aby go używać należy go skonfigurować jak w projekcie ClientHost:

* App.config nie jest potrzebne
* Zależności (IServer, IGame) można rozwiązać jak w klasie Program.cs w wymienionym projekcie.
  Albo użyć jakiegoś automatu, lub dodać własną klasę konfigurującą kontener. To tylko przykład co zrobić, żeby w ogóle działało.

TODO
----
* komunikacja między klientami poprzez serwer
* obsługa adresu podanego na wejście jako adres dla serwera
