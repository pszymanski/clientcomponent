﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Contracts;
using Server;

namespace Client
{
    public class Client : IClient
    {
        public Server.IServerService server;

        public Client()
        {
            var factory = new ChannelFactory<Server.IServerService>(new NetTcpBinding(), "net.tcp://localhost:9000");
            server = factory.CreateChannel();
            server.GetIP(); //sprawdzenie, czy serwer stoi (timeout)
        }

        public Client(String serverAddress)
        {
            var factory = new ChannelFactory<Server.IServerService>(new NetTcpBinding(), serverAddress);
            server = factory.CreateChannel();
            server.GetIP(); //sprawdzenie, czy serwer stoi (timeout)
        }

        public void Update(IPacket state)
        {
            throw new NotImplementedException();
        }

        public IPacket Ping()
        {
            return new Packets.NullPacket();
        }

        public void Send(IPacket state)
        {
            throw new NotImplementedException();
        }

        public bool IWannaPlay(int OpponentID)
        {
            return server.WannaPlay(OpponentID);
        }


        public int Connect(string login, string password, String game)
        {
            int id= server.LogIn(login, password, game);
            if(id==-1)
                id=server.RegisterUser(login,password, game);
            server.Connected(id);
            return id;
        }


        public string Test(String wtv)
        {
            return server.GetIP();
        }


        public void SetServer(IServerService server)
        {
            this.server = server;
        }


        public void ChangeRanking(string login, string password, string game, int delta)
        {
            int id = server.RegisterUser(login, password, game);
            server.Connected(id);
            server.SetRanking(id, delta);
        }


        public Dictionary<Int32, Int32> Ranking()
        {
            return server.GetRanking();
        }


        public string Whois(int id)
        {
            return server.Whois(id);
        }
    }
}

