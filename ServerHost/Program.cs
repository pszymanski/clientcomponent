﻿using System;
using System.ServiceModel;

namespace ServerHost
{
    class Program
    {
        delegate void OpenHost();

        static void Main(string[] args)
        {
            Uri uri;

            switch (args.Length)
            {
                case 0:
                    uri = new Uri("net.tcp://localhost:9000");
                    break;
                case 1:
                    uri = new Uri("net.tcp://"+args[0]);
                    break;
                default:
                    Console.WriteLine("Usage: ServerHost [address:port]\n\t[address:port] - Listen address for server. (Default: localhost:9000)");
                    return;
            }

            ServiceHost host = new ServiceHost(typeof(Server.ServerService), uri);
            host.AddServiceEndpoint("Server.IServerService", new NetTcpBinding(), "");

            try
            {
                host.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            Console.WriteLine(host.BaseAddresses[0].ToString()+"\nPress a key to close.");
            Console.ReadKey();
            host.Close();
        }
    }
}

