﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Packets
{
    public class NullPacket : Contracts.IPacket
    {
        public string Type()
        {
            return "Null";
        }

        public string GameType()
        {
            return "General";
        }

        public object Data()
        {
            return null;
        }
    }
}
