﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    public class Server
    {
        public string ID;
        public string state;
        [DataMember]
        public bool connect;
        [DataMember]
        public bool move;
        [DataMember]
        public bool unoccupied;
        [DataMember]
        public bool wannaplay;
    }
}
