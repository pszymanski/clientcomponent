﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Net;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Server
{
    [ServiceBehavior]
    public class ServerService : IServerService
    {
        public int ID;
        public string state;
        public bool connect;
        public bool move;
        public bool unoccupied;
        public bool wannaplay;
        Dictionary<Int32, String> Players = new Dictionary<Int32, String>() { };
        Dictionary<Int32, Int32> Ranking = new Dictionary<Int32, Int32>() { };
        public MySqlConnect conx = new MySqlConnect();
        MySqlCommand cmd;
        string sql;

        public string GetIP()
        {
            string host_name = Dns.GetHostName();
            IPHostEntry ip_host_entry = Dns.GetHostEntry(host_name);
            IPAddress[] ip_address = ip_host_entry.AddressList;
            return ip_address[ip_address.Length - 1].ToString();
        }

        /*
         *Metoda Connected ma być włączana w momencie logowania i wyłączania aplikacji zamienia ona wartość w bazie danych
         *odpowiadającą czy uzytkownik jest zalogowany 0 - nie zalogowany 1 - zalogowany. Teraz wystarczy jedynie sprawdzac tą wartość
         *w miejscach które tego potrzebują.
         */
        public void Connected(int ID)
        {
            conx.Connect();
            int OnlinePlayers;
            object wyniczek;
            sql = "SELECT `Online` FROM `mspage1`.`Online` WHERE  `Online`.`ID` =" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            connect = Convert.ToInt32(result) == 0 ? false : true;
            if (connect)
            {
                sql = "UPDATE  `mspage1`.`Online` SET  `Online` =  '0' WHERE  `Online`.`ID` =" + ID;
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
                sql = "SELECT `OnlinePlayers` FROM `mspage1`.`Informations`";
                cmd = new MySqlCommand(sql, conx.conn);
                wyniczek = cmd.ExecuteScalar();
                OnlinePlayers = Convert.ToInt32(wyniczek) - 1;
                sql = "UPDATE  `mspage1`.`Informations` SET  `OnlinePlayers` =  '" + OnlinePlayers + "'";
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
            }
            else
            {
                sql = "UPDATE  `mspage1`.`Online` SET  `Online` =  '1' WHERE  `Online`.`ID` =" + ID;
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
                sql = "SELECT `OnlinePlayers` FROM `mspage1`.`Informations`";
                cmd = new MySqlCommand(sql, conx.conn);
                wyniczek = cmd.ExecuteScalar();
                OnlinePlayers = Convert.ToInt32(wyniczek) + 1;
                sql = "UPDATE  `mspage1`.`Informations` SET  `OnlinePlayers` =  '" + OnlinePlayers + "'";
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
            }
        }
        /*
         * Metoda CanMove() tak samo jak Connected zmienia wartość odpowiadająca pozwoleniu na ruch
         * wykonanie jej pozwala na Ruch.
         * przed wykonaniem ruchu powinno się sprawdzic w bazie czy mozna wykonac ruch
         */
        public void CanMove()
        {
            conx.Connect();
            sql = "SELECT `CanMove` FROM `mspage1`.`Online` WHERE  `Online`.`ID` =" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            move = Convert.ToInt32(result) == 0 ? false : true;
            if (move)
            {
                sql = "UPDATE  `mspage1`.`Online` SET  `CanMove` =  '0' WHERE  `Online`.`ID` =" + ID;
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
            }
            else
            {
                sql = "UPDATE  `mspage1`.`Online` SET  `CanMove` =  '1' WHERE  `Online`.`ID` =" + ID;
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
            }
        }
        /*
         * Metoda Unoccupied() sprawdza w bazie czy gracz aktualnie jest zajety inna gra
         * przy tworzeniu gry stan occupied przy danym graczu zmienia się na zajety
         */
        public string Unoccupied()
        {
            conx.Connect();
            sql = "SELECT `Occupied` FROM `mspage1`.`Online` WHERE  `Online`.`ID` =" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            state = Convert.ToInt32(result) == 0 ? "InLobby" : "playing";
            return state;

        }
        /*
         * Metoda WannaPlay() zmienia w bazie przy przeciwniku wartość pytającego z 0 na nr ID pytajacego.
         * w Lobby powinnabyć metoda nasłuchująca zmian na tej pozycji i w razie zmiany wyrzucająca alert o zapytaniu.
         * Nie pozwala także zapytać jeżeli zawodnik aktualnie gra.
         */
        public bool WannaPlay(int OpponentID)
        {
            conx.Connect();
            state = Unoccupied();
            if (state == "InLobby")
            {
                sql = "UPDATE  `mspage1`.`Online` SET  `Ask` =  '" + OpponentID + "' WHERE  `Online`.`ID` =" + ID;
                cmd = new MySqlCommand(sql, conx.conn);
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }

        }
        //public LobbyService() 
        //{
        //    setActualNumberOfGames();
        //}
        //public void setActualNumberOfGames() 
        //{
        //    conx.Connect();
        //    sql = "SELECT count(*) FROM `information_schema`.`TABLES` WHERE `TABLES`.`TABLE_SCHEMA` = 'mspage1';";
        //    cmd = new MySqlCommand(sql, conx.conn);
        //    object result = cmd.ExecuteScalar();
        //    int Number = Convert.ToInt32(result) - 4/*4 to suma tabel USERS, RANK, INFORMATION i ONLINE */;
        //    this.ActualNumberOfGames = Number;
        //}
        public Dictionary<Int32, String> GetPlayers(String game)/*Jako parametr game użyjcie "Warcaby", "Scorch" lub "Tetris"*/
        {
            conx.Connect();
            List<String> tmp = new List<String>() { };
            Players.Clear();
            sql = "SELECT `Login` FROM `mspage1`.`Users` WHERE `Users`.`Game`='" + game + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            MySqlDataReader reader;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string login = reader[0].ToString();
                tmp.Add(login);
            }
            reader.Close();
            foreach (String login in tmp)
            {
                sql = "SELECT `ID` FROM `mspage1`.`Users` WHERE `Users`.`Login`='" + login + "'";
                cmd = new MySqlCommand(sql, conx.conn);
                object result = cmd.ExecuteScalar();
                int id = Convert.ToInt32(result);
                if (!Players.Keys.Contains(id))
                    Players.Add(id, login);
            }
            return Players;
        }
        public Dictionary<Int32, Int32> GetRanking()/*Jako parametr game użyjcie "Warcaby", "Scorch" lub "Tetris"*/
        {
            conx.Connect();
            Ranking.Clear();
            if (Players.Keys.Count != 0)
            {
                foreach (Int32 id in Players.Keys)
                {
                    sql = "SELECT `Points` FROM `mspage1`.`Rank` WHERE `Rank`.`ID`=" + id;
                    cmd = new MySqlCommand(sql, conx.conn);
                    object result = cmd.ExecuteScalar();
                    int points = Convert.ToInt32(result);
                    Ranking.Add(id, points);
                }
            }
            return Ranking;
        }
        public void SetRanking(int ID, int Points)/*Dodaje użytkownikowi o danym ID punkty podane w Points do tych które już posiada, Points może być ujemne*/
        {
            conx.Connect();
            sql = "SELECT `Points` FROM `mspage1`.`Rank` WHERE `ID`=" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            int ActualPoints = (int)result;
            int NewPoints = ActualPoints + Points;
            sql = "UPDATE  `mspage1`.`Rank` SET  `Points` =  " + NewPoints + " WHERE `ID`=" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
        }
        public int RegisterUser(String login, String haslo, String game) /*Zwraca Przypisane ID zarejestrowanego użytkownika jako parametr game użyjcie "Warcaby", "Scorch" lub "Tetris"*/
        {
            conx.Connect();
            sql = "INSERT INTO `mspage1`.`Users` (`ID`, `Login`, `Password`, `Game`) VALUES (NULL, '" + login + "', '" + haslo + "','" + game + "')";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "SELECT `ID` FROM `mspage1`.`Users` WHERE `Users`.`Login`='" + login + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            int id = Convert.ToInt32(result);
            sql = "INSERT INTO `mspage1`.`Online` (`ID`, `Online`, `Occupied`, `CanMove`, `Ask`) VALUES (" + id + ", 0, 0, 0, 0 )";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "INSERT INTO `mspage1`.`Rank` (`ID`, `Points`) VALUES (" + id + ", 0)";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "SELECT `NumberOfPlayers` FROM `mspage1`.`Informations`";
            cmd = new MySqlCommand(sql, conx.conn);
            result = cmd.ExecuteScalar();
            int Players = Convert.ToInt32(result) + 1;
            sql = "UPDATE  `mspage1`.`Informations` SET  `NumberOfPlayers` =  '" + Players + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "SELECT `ID` FROM `mspage1`.`Users` WHERE `Login`='" + login + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            ID = Convert.ToInt32(cmd.ExecuteScalar());
            return ID;            
        }
        public int LogIn(String login, String haslo, String game)/*Logowanie zwraca ID zalogowanego jeżeli zalogował lub 0 jeżeli jest złe hasło lub -1 jeżeli użytkownik nie istnieje */
        {
            conx.Connect();
            int ID;
            MySqlDataReader reader;
            sql = "SELECT `Password` FROM `mspage1`.`Users` WHERE `Login`='" + login + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                String result = reader[0].ToString();
                reader.Close();
                sql = "SELECT `ID` FROM `mspage1`.`Users` WHERE `Login`='" + login + "'";
                cmd = new MySqlCommand(sql, conx.conn);
                ID = Convert.ToInt32(cmd.ExecuteScalar());
                if (haslo.Equals(result))
                {
                    Connected(ID);
                }
                else
                {
                    ID = 0;
                }
            }
            else
            {
                ID = -1;
                if (game == "Scorch")
                {
                    RegisterUser(login, haslo, game);
                }
            }
            return ID;
        }
        public void LogOut(int ID)
        {
            Connected(ID);
        }
        public int BeginGame(int WhitePlayerID, int BlackPlayerID)
        {
            conx.Connect();
            sql = "SELECT `GamesDone` FROM `mspage1`.`Informations`";
            cmd = new MySqlCommand(sql, conx.conn);
            object result = cmd.ExecuteScalar();
            int WhichGame = Convert.ToInt32(result);
            sql = "CREATE TABLE IF NOT EXISTS `Game" + WhichGame + "` (";
            sql += "`ID` int(11) NOT NULL,";
            sql += "`MyTurn` int(1) NOT NULL,";
            sql += "`XBegin` int(11) NOT NULL,";
            sql += "`YBegin` int(11) NOT NULL,";
            sql += "`XDestination` int(11) NOT NULL,";
            sql += "`YDestination` int(11) NOT NULL,";
            sql += "`Whacking` int(1) NOT NULL,";
            sql += "`XWhacking` int(11) NOT NULL,";
            sql += "`YWhacking` int(11) NOT NULL";
            sql += ") ENGINE=InnoDB DEFAULT CHARSET=latin2;";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "INSERT INTO `mspage1`.`Game" + WhichGame + "` (`ID`, `MyTurn`, `XBegin`, `YBegin`, `XDestination`, `YDestination`, `Whacking`, `XWhacking`, `YWhacking`) VALUES ('" + WhitePlayerID + "', '1', '0', '0', '0', '0', '0', '0', '0'), ('" + BlackPlayerID + "', '0', '0', '0', '0', '0', '0', '0', '0');";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "SELECT `GamesDone` FROM `mspage1`.`Informations`";
            cmd = new MySqlCommand(sql, conx.conn);
            result = cmd.ExecuteScalar();
            int Games = Convert.ToInt32(result) + 1;
            sql = "UPDATE  `mspage1`.`Informations` SET  `GamesDone` =  '" + Games + "'";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();

            return WhichGame;
        }
        public void EndGame(int WhichGame)
        {
            sql = "DROP TABLE `mspage1`.`Game" + WhichGame + "`";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
        }
        public void SendMove(int PlayerID, int OpponentID, int WhichGame, int XBegin, int YBegin, int XDestination, int YDestination, bool Whacking, int XWhacking, int YWhacking)
        {
            int Whack = Whacking ? 1 : 0;
            sql = "UPDATE  `mspage1`.`Game" + WhichGame + "` SET  `MyTurn` =  0,";
            sql += "`XBegin` =  " + XBegin + ",";
            sql += "`YBegin` =  " + YBegin + ",";
            sql += "`XDestination` =  " + XDestination + ",";
            sql += "`YDestination` =  " + YDestination + ",";
            sql += "`Whacking` =  " + Whack + ",";
            sql += "`XWhacking` =  " + XWhacking + ",";
            sql += "`YWhacking` =  " + YWhacking + " WHERE  `Game" + WhichGame + "`.`ID` =" + PlayerID + " ;";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
            sql = "UPDATE  `mspage1`.`Game" + WhichGame + "` SET  `MyTurn` =  1 WHERE  `Game" + WhichGame + "`.`ID` =" + OpponentID + " ;";
            cmd = new MySqlCommand(sql, conx.conn);
            cmd.ExecuteNonQuery();
        }

        public string Whois(int ID)
        {
            conx.Connect();
            sql = "SELECT `Login` FROM `mspage1`.`Users` WHERE `Users`.`ID` =" + ID;
            cmd = new MySqlCommand(sql, conx.conn);
            MySqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            //conx.Disconnect();
            return reader[0].ToString();

        }
    }
}
