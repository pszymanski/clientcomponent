﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Server
{
    [ServiceContract]
    public interface IServerService
    {
        [OperationContract]
        void Connected(int ID);
        [OperationContract]
        void CanMove();
        [OperationContract]
        string Unoccupied();
        [OperationContract]
        bool WannaPlay(int OpponentID);
        [OperationContract]
        string GetIP();
        [OperationContract]
        Dictionary<Int32, String> GetPlayers(String game);
        [OperationContract]
        Dictionary<Int32, Int32> GetRanking();
        [OperationContract]
        void SetRanking(int ID, int Points);
        [OperationContract]
        int RegisterUser(String login, String haslo, String game);
        [OperationContract]
        int LogIn(String login, String haslo, String game);
        [OperationContract]
        void LogOut(int ID);
        [OperationContract]
        int BeginGame(int WhitePlayerID, int BlackPlayerID);
        [OperationContract]
        void EndGame(int WhichGame);
        [OperationContract]
        void SendMove(int PlayerID, int OpponentID, int WhichGame, int XBegin, int YBegin, int XDestination, int YDestination, bool Whacking, int XWhacking, int YWhacking);
        [OperationContract]
        string Whois(int ID);
        

    }
}
